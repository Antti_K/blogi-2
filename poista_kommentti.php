<?php include_once 'inc/top.php';?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
        <?php
        if ($tietokanta!=null) {
            $id = filter_input(INPUT_GET, 'id',FILTER_SANITIZE_NUMBER_INT);
            $kirjoitus_id = filter_input(INPUT_GET, 'kirjoitus_id',FILTER_SANITIZE_NUMBER_INT);
            try {
                
                $kysely = $tietokanta->prepare("DELETE FROM kommentti WHERE id=:id");
                $kysely->bindValue(':id', $id, PDO::PARAM_INT);
                $kysely->execute();                
      
                print "<p>Kommentti poistettu.</p>";
                print "<a href='blogi.php?id=$kirjoitus_id'>Takaisin kirjoitukseen</a>";

            } catch (PDOException $pdoex) {
                print "Kirjoitusten hakeminen epäonnistui." . $pdoex->getMessage();
            }
        }
        ?>
        </div>
    </div>
</div>
<?php include_once 'inc/bottom.php';?>
