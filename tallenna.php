<?php include_once 'inc/top.php';
if (!isset($_SESSION['login'])) {
    header('Location: login.php');
}
$id=0;
$otsikko="";
$teksti="";
?>
<div class="container">
    <div class="row">
        <?php
        if ($_SERVER['REQUEST_METHOD']=='GET') {
            if (isset($_GET['id'])) {
                $id=filter_input(INPUT_GET,"id",FILTER_SANITIZE_NUMBER_INT);

                $sql='SELECT * FROM kirjoitus WHERE id=' . $id;
                $kysely=$tietokanta->query($sql);  
                $kysely->setFetchMode(PDO::FETCH_OBJ);
                if ($kysely->rowCount()==1) {
                    $tietue = $kysely->fetch();
                    $otsikko=$tietue->otsikko;
                    $teksti=$tietue->teksti;
                }
                else {
                    print "<p>Kirjoitusta ei löydy, tietoja ei voi enää muuttaa!</p>";
                }
            }
        }
        else if ($_SERVER['REQUEST_METHOD']=='POST') {
            try {
                $id=filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);
                $otsikko=filter_input(INPUT_POST,'otsikko',FILTER_SANITIZE_STRING);
                $teksti=filter_input(INPUT_POST,'teksti',FILTER_SANITIZE_STRING);
                $kayttaja_id=$_SESSION['kayttaja_id'];

                if ($id==0) {
                    $kysely = $tietokanta->prepare("INSERT INTO kirjoitus (otsikko, teksti,kayttaja_id) "
                        . "VALUES (:otsikko,:teksti,:kayttaja_id)");

                }
                else {
                    $kysely = $tietokanta->prepare("UPDATE kirjoitus SET otsikko=:otsikko,teksti=:teksti,kayttaja_id=:kayttaja_id  "
                        . "WHERE id=:id");
                      $kysely->bindValue(':id', $id, PDO::PARAM_INT);
                }
                
                $kysely->bindValue(':otsikko', $otsikko, PDO::PARAM_STR);
                $kysely->bindValue(':teksti', $teksti, PDO::PARAM_STR);
                $kysely->bindValue(':kayttaja_id', $kayttaja_id, PDO::PARAM_INT);
                
                $kysely->execute(); 
                $id=$tietokanta->lastInsertId();
                
                print "<div class='col-xs-12'>";
                print "<p>Tiedot tallennettu.</p>";
                print "<a href='index.php'>Takaisin etusivulle</a></div>";

            } catch (PDOException $pdoex) {
                print "<p class='virhe'>Kirjoituksen tallennuksessa tapahtui virhe." . $pdoex->getMessage();
            }
        }
        ?>
        <div class="col-xs-12">
            <?php
            if ($id==0) {
            ?>
                <h3>Lisää kirjoitus</h3>
            <?php
            }
            else {
            ?>
                <h3>Muokkaa kirjoitusta</h3>
            <?php
            }
            ?>
            <form role="form" action="<?php print($_SERVER['PHP_SELF']); ?>" method="post">
                <input name="id" type="hidden" value="<?php print($id);?>">
                <div class="form-group">
                    <label for="otsikko">Otsikko</label>
                    <input class="form-control" id="otsikko" name="otsikko" maxlength="50" 
                           value="<?php print($otsikko);?>" placeholder="Otsikko tähän" required>
                </div>
                <div class="form-group">
                    <label for="teksti">Teksti</label>
                    <textarea class="form-control" id="teksti" name="teksti" required placeholder="Teksti tänne..."><?php print($teksti);?></textarea>
                </div>
                <div class="form-inline">
                    <button type="submit" class="btn btn-primary">Tallenna</button>
                    <a href="index.php" class="btn btn-default">Peruuta</a>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include_once 'inc/bottom.php';?>