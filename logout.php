<?php
session_start();
unset($_SESSION['login']);
unset($_SESSION['kayttaja_id']);
session_destroy();
header('Location: index.php');

