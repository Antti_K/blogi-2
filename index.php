<?php 
include_once 'inc/top.php';
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
        <?php
        if ($tietokanta!=null) {
            try {                     
                //$sql='SELECT * FROM kirjoitus ORDER BY paivays desc';
                $sql = "SELECT *,kirjoitus.id as id FROM kirjoitus INNER JOIN kayttaja ON kirjoitus.kayttaja_id = kayttaja.id"
                        . " ORDER BY paivays desc";
      
                $kysely=$tietokanta->query($sql);  
                $kysely->setFetchMode(PDO::FETCH_OBJ);
                
                while($tietue = $kysely->fetch()) {
                    print '<div class="kirjoitus">';
                    print "<p>";
                    print date("d.m.Y H.i",  strtotime($tietue->paivays));
                    print " by " . $tietue->tunnus . "<br />";
                   
                    print "<a href='blogi.php?id=$tietue->id'>$tietue->otsikko</a> ";
                    if (isset($_SESSION['kayttaja_id'])) {
                        print "<a href='poista.php?id=$tietue->id'><span class='glyphicon glyphicon-trash'></span></a>";     
                    }
                    print "</p>";     
                    print "</div>";
                }

            } catch (PDOException $pdoex) {
                print "<p>Häiriö tietokantayhteydessä." . $pdoex->getMessage() . "</p>";
            }
        }
        ?>
        </div>
    </div>
</div>
<?php include_once 'inc/bottom.php';?>