<?php 
include_once 'inc/top.php';
$viesti = "";
if ($_SERVER['REQUEST_METHOD'] ==='POST') {
    if ($tietokanta!=null) {
        try {
            $tunnus = filter_input(INPUT_POST,'tunnus',FILTER_SANITIZE_STRING);
            $salasana = md5(filter_input(INPUT_POST,'salasana',FILTER_SANITIZE_STRING));

            $sql="SELECT * FROM kayttaja where tunnus='$tunnus' AND salasana='$salasana'";
            
            $kysely = $tietokanta->query($sql);
       
            if ($kysely->rowCount()===1) {
                $tietue = $kysely->fetch();
                $_SESSION['login'] = true;
                $_SESSION['kayttaja_id'] = $tietue['id'];
                header('Location: index.php');
            }
            else {
                $viesti = "Väärä tunnus tai salasana!";
            }

        } catch (PDOException $pdoex) {
            $viesti = "Kayttajan tietojen hakeminen epäonnistui." . $pdoex->getMessage();
        }
    }
}
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <p><?php print($viesti);?></p>
            <form method="post" action="<?php print $_SERVER['PHP_SELF']?>">
                <div class="form-group">
                    <label for="tunnus">Tunnus:</label>
                    <input type="tunnus" class="form-control" id="tunnus" name="tunnus">
                </div>
                <div class="form-group">
                    <label for="salasana">Salasana:</label>
                    <input type="password" class="form-control" id="salasana" name="salasana">
                </div>              
                <button type="submit" class="btn btn-default">Kirjaudu</button>
            </form>
        </div>
    </div>
</div>
<?php include_once 'inc/bottom.php';?>